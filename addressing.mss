#housenumbers {
  [zoom >= 18] {
    text-name: [housenumber];
    [housenumber != null][housename != null] {
      text-name: [housenumber] + '\n' + [housename];
    }
    [housenumber = null][housename != null] {
      text-name: [housename];
    }
    text-placement: interior;
    text-min-distance: 1;
    text-wrap-width: 0;
    text-face-name: @book-fonts;
    text-fill: @housenumbers-color;
    text-halo-fill: @housenumbers-halo-fill;
    text-halo-radius: 1.5;
    text-size: 9;

    [zoom >= 19] {
      text-size: 10;
    }
  }
}
