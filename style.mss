Map {
  background-color: @land-color;
}

/*
---- fonts
*/

@book-fonts:    "DejaVu Sans Book", "Arundina Sans Regular", "Padauk Regular", "Khmer OS Metal Chrieng Regular",
                "Mukti Narrow Regular", "gargi Medium", "TSCu_Paranar Regular", "Tibetan Machine Uni Regular", "Mallige Normal",
                "Droid Sans Fallback Regular", "Unifont Medium", "unifont Medium";
@bold-fonts:    "DejaVu Sans Bold", "Arundina Sans Bold", "Padauk Bold", "Mukti Narrow Bold", "TSCu_Paranar Bold", "Mallige Bold",
                "DejaVu Sans Book", "Arundina Sans Regular", "Padauk Regular", "Khmer OS Metal Chrieng Regular",
                "Mukti Narrow Regular", "gargi Medium", "TSCu_Paranar Regular", "Tibetan Machine Uni Regular", "Mallige Normal",
                "Droid Sans Fallback Regular", "Unifont Medium", "unifont Medium";

@oblique-fonts: "DejaVu Sans Oblique", "Arundina Sans Italic", "TSCu_Paranar Italic", "Mallige NormalItalic",
                "DejaVu Sans Book", "Arundina Sans Regular", "Padauk Regular", "Khmer OS Metal Chrieng Regular",
                "Mukti Narrow Regular", "gargi Medium", "TSCu_Paranar Regular", "Tibetan Machine Uni Regular", "Mallige Normal",
                "Droid Sans Fallback Regular", "Unifont Medium", "unifont Medium";

/*
---- base
*/

@water-color: husl(232, 40%, 72%);
@land-color: husl(86, 4%, 95%);

/*
---- landcover
*/

@forest: husl(118, 20%, 85%);
@forest-text: darken(@forest, 40%);
@forest-line: darken(@forest, 15%);
@trees: darken(@forest, 5%);
@tree-trunk: darken(@land-color, 40%);
@grass: husl(110, 38%, 90%); // TODO phase out - used for text rendering
@scrub: lighten(@forest, 4%);
@scrub-line: darken(@scrub, 15%);
@green: lighten(@forest, 7%);
@green-line: darken(@green, 15%);
@barrier: darken(spin(@land-color, -20), 45%);

@pitch: husl(77, 7%, 90.5%);

@builtup: darken(@land-color, 4%);
@builtup-line: darken(@builtup, 7%);

@bare_ground: husl(57, 26.5%, 92%);
@cemetery: husl(133, 23%, 79%); // also grave_yard
@danger_area: pink;
@mud: husla(49, 27%, 74%, 0.3);
@sand: husl(68, 25%, 90%);
@tourism: husl(47 ,97%, 35%);
@quarry: husl(0, 0%, 88%);
@military: husl(12, 100%, 61%);
@beach: husl(83, 20.5%, 91.5%);

/*
---- buildings
*/

@building-line-width-z14: .5;
@building-line-width: 1;

@building-fill: husl(71, 9%, 85%);
@building-construction-fill: lighten(@building-fill, 3%);
@building-ruins-fill: @building-construction-fill;
@building-fill-z14: lighten(@building-fill, 2%);
@building-construction-fill-z14: lighten(@building-construction-fill, 2%);
@building-ruins-fill-z14: @building-construction-fill-z14;

@building-line-z14: darken(@building-fill-z14, 15%);

@building-line-z15: darken(@building-fill, 7%);
@building-line-z16: darken(@building-fill, 12%);
@building-line-z17: darken(@building-fill, 20%);

@building-major-fill: saturate(darken(spin(@building-fill, -5), 10%), 10%);

@building-major-line-z15: darken(@building-major-fill, 5%);
@building-major-line-z16: darken(@building-major-fill, 10%);
@building-major-line-z17: darken(@building-major-fill, 20%);

/*
---- addresses
*/

@housenumbers-color: darken(@building-fill, 50%);
@housenumbers-halo-fill: husla(0, 100%, 100%, 0.3);

/*
---- admin
*/

@admin-boundaries: husl(308, 70%, 48%);
@national-park-boundaries: darken(@forest, 40%);
@national-park-boundaries-text: darken(@national-park-boundaries, 10%);

/*
---- ferry routes
*/

@ferry-route: darken(@water-color, 40%);
@ferry-route-text: @ferry-route;

/*
---- placenames
*/

@placenames: husl(0, 0%, 13%);
@placenames-light: lighten(@placenames, 34%);

@country-labels: darken(@admin-boundaries, 15%);
@state-labels: desaturate(darken(@admin-boundaries, 5%), 20%);

/*
---- poi
*/

@poi-color: husl(0, 0%, 31%);

@marina-text: husl(262, 78%, 50%); // also swimming_pool
@wetland-text: husl(253, 99%, 44%); /* Also for marsh and mud */
@transportation-icon: husl(243, 100%, 58%);

/*
---- power
*/

@power-line-color: husl(0, 0%, 53%);

/*
---- roads and railways
*/

@motorway-fill: husl(1, 68%, 70%);
@trunk-fill: husl(27, 88%, 79%);
@primary-fill: husl(55, 90%, 89%);
@secondary-fill: husl(88, 42%, 97%);

@motorway-casing: darken(saturate(@motorway-fill, 10%), 20%);
@trunk-casing: darken(@trunk-fill, 30%);
@primary-casing: darken(@primary-fill, 40%);
@secondary-casing: darken(@secondary-fill, 50%);

@motorway-low-zoom: husl(0.5, 70%, 61.5%);
@motorway-z5: lighten(@motorway-low-zoom, 12%);
@motorway-z6: lighten(@motorway-low-zoom, 7%);
@motorway-z7: lighten(@motorway-low-zoom, 3%);

@trunk-low-zoom: husl(24, 92%, 71%);
@trunk-z5: lighten(@trunk-low-zoom, 12%);
@trunk-z6: lighten(@trunk-low-zoom, 7%);
@trunk-z7: lighten(@trunk-low-zoom, 3%);

@primary-low-zoom: husl(53, 83%, 81.5%);
@secondary-low-zoom: husl(91, 65%, 92%);

@motorway-low-zoom-casing: desaturate(darken(@motorway-low-zoom, 20%), 10%);
@trunk-low-zoom-casing: desaturate(darken(@trunk-low-zoom, 15%), 20%);
@primary-low-zoom-casing: saturate(darken(@primary-low-zoom, 20%), 10%);
@secondary-low-zoom-casing: saturate(darken(@secondary-low-zoom, 20%), 30%);

@shield-motorway-fill: darken(@motorway-fill, 55%);
@shield-trunk-fill: darken(@trunk-fill, 60%);
@shield-primary-fill: darken(@primary-fill, 65%);
@shield-secondary-fill: darken(@secondary-fill, 73%);
@shield-tertiary-fill: darken(@tertiary-fill, 73%);

@tertiary-fill: white;
@residential-fill: white;
@service-fill: @residential-fill;
@pedestrian-fill: husl(0, 0%, 93%);
@raceway-fill: @aeroway-fill;
@road-fill: husl(0, 0%, 87%);
@footway-fill: salmon;
@steps-fill: @footway-fill;
@cycleway-fill: blue;
@bridleway-fill: green;
@track-fill: husl(48, 100%, 47%);
@aeroway-fill: darken(@builtup, 5%);
@runway-fill: @aeroway-fill;
@taxiway-fill: @aeroway-fill;

@platform-fill: desaturate(darken(@builtup, 10%), 10%);
@platform-line: darken(@platform-fill, 20%);

@default-casing: @land-color;
@tertiary-casing: darken(@tertiary-fill, 44%);
@residential-casing: darken(@residential-fill, 27%);
@road-casing: @residential-casing;
@service-casing: @residential-casing;
@pedestrian-casing: @residential-casing;
@path-casing: @default-casing;
@footway-casing: @default-casing;
@steps-casing: @default-casing;
@cycleway-casing: @default-casing;
@bridleway-casing: @default-casing;
@track-casing: @default-casing;

@bridge-casing: darken(@land-color, 60%);
@motorway-bridge-casing: darken(@motorway-casing, 30%);
@trunk-bridge-casing: darken(@trunk-casing, 20%);
@primary-bridge-casing: darken(@primary-casing, 15%);
@secondary-bridge-casing: darken(@secondary-casing, 10%);
@tertiary-bridge-casing: darken(@tertiary-casing, 30%);
@residential-bridge-casing: darken(@residential-casing, 30%);
@road-bridge-casing: @residential-bridge-casing;
@service-bridge-casing: @residential-bridge-casing;
@living-street-bridge-casing: @residential-bridge-casing;
@pedestrian-bridge-casing: darken(@pedestrian-casing, 30%);

@unimportant-road: @residential-casing;

@residential-construction: husl(0, 0%, 67%);
@service-construction: @residential-construction;

@destination-marking: husl(241, 100%, 88%);
@private-marking: husl(12, 72%, 76%);
@private-marking-for-red: husl(12, 39%, 54%);

@tunnel-casing: grey;

@motorway-tunnel-fill: lighten(@motorway-fill, 10%);
@trunk-tunnel-fill: lighten(@trunk-fill, 10%);
@primary-tunnel-fill: lighten(@primary-fill, 10%);
@secondary-tunnel-fill: lighten(@secondary-fill, 5%);
@tertiary-tunnel-fill: lighten(@tertiary-fill, 5%);
@residential-tunnel-fill: darken(@residential-fill, 5%);
@living-street-tunnel-fill: lighten(@residential-fill, 10%);

@motorway-width-z5:               0.5;
@trunk-width-z5:                  0.4;

@motorway-width-z7:               0.8;
@trunk-width-z7:                  0.6;

@motorway-width-z8:               1;
@trunk-width-z8:                  1;
@primary-width-z8:                1;

@motorway-width-z9:               1.4;
@trunk-width-z9:                  1.4;
@primary-width-z9:                1.4;
@secondary-width-z9:              1;

@motorway-width-z10:              1.9;
@trunk-width-z10:                 1.9;
@primary-width-z10:               1.8;
@secondary-width-z10:             1;

@motorway-width-z11:              2.0;
@trunk-width-z11:                 1.9;
@primary-width-z11:               1.8;
@secondary-width-z11:             1;

@motorway-width-z12:              3.5;
@motorway-link-width-z12:         1.5;
@trunk-width-z12:                 3.5;
@primary-width-z12:               3.5;
@secondary-width-z12:             3.5;
@tertiary-width-z12:              2.5;

@motorway-width-z13:              6;
@motorway-link-width-z13:         4;
@trunk-width-z13:                 6;
@primary-width-z13:               5;
@secondary-width-z13:             5;
@tertiary-width-z13:              4;
@residential-width-z13:           2.5;
@living-street-width-z13:         2;
@pedestrian-width-z13:            2;
@bridleway-width-z13:             0.3;
@footway-width-z13:               0.7;
@cycleway-width-z13:              0.7;
@path-width-z13:                  0.2;
@track-width-z13:                 0.5;
@track-grade1-width-z13:          0.5;
@track-grade2-width-z13:          0.5;
@steps-width-z13:                 0.7;

@secondary-width-z14:             5;
@tertiary-width-z14:              5;
@residential-width-z14:           3;
@living-street-width-z14:         3;
@pedestrian-width-z14:            3;
@road-width-z14:                  2;
@service-width-z14:               2;

@motorway-width-z15:             10;
@motorway-link-width-z15:         7.8;
@trunk-width-z15:                10;
@primary-width-z15:              10;
@secondary-width-z15:             9;
@tertiary-width-z15:              9;
@residential-width-z15:           5;
@living-street-width-z15:         5;
@pedestrian-width-z15:            5;
@service-width-z15:               2.5;
@bridleway-width-z15:             1.2;
@footway-width-z15:               1;
@cycleway-width-z15:              0.9;
@path-width-z15:                  0.5;
@track-width-z15:                 1.5;
@track-grade1-width-z15:          0.75;
@track-grade2-width-z15:          0.75;
@steps-width-z15:                 3;

@secondary-width-z16:            10;
@tertiary-width-z16:             10;
@residential-width-z16:           6;
@living-street-width-z16:         6;
@pedestrian-width-z16:            6;
@road-width-z16:                  3.5;
@service-width-z16:               4;
@minor-service-width-z16:         2;
@footway-width-z16:               1.3;
@cycleway-width-z16:              0.9;

@motorway-width-z17:             18;
@motorway-link-width-z17:        12;
@trunk-width-z17:                18;
@primary-width-z17:              18;
@secondary-width-z17:            18;
@tertiary-width-z17:             18;
@residential-width-z17:          12;
@living-street-width-z17:        12;
@pedestrian-width-z17:           12;
@road-width-z17:                  7;
@service-width-z17:               7;
@minor-service-width-z17:         3.5;

@motorway-width-z18:             21;
@motorway-link-width-z18:        13;
@trunk-width-z18:                21;
@primary-width-z18:              21;
@secondary-width-z18:            21;
@tertiary-width-z18:             21;
@residential-width-z18:          13;
@living-street-width-z18:        13;
@pedestrian-width-z18:           13;
@road-width-z18:                  8.5;
@service-width-z18:               8.5;
@minor-service-width-z18:         4.75;

@motorway-width-z19:             27;
@motorway-link-width-z19:        16;
@trunk-width-z19:                27;
@primary-width-z19:              27;
@secondary-width-z19:            27;
@tertiary-width-z19:             27;
@residential-width-z19:          17;
@living-street-width-z19:        17;
@pedestrian-width-z19:           17;
@road-width-z19:                 11;
@service-width-z19:              11;
@minor-service-width-z19:         5.5;

@footway-width-z18:               1.3;
@cycleway-width-z18:              1;

@footway-width-z19:               1.6;
@cycleway-width-z19:              1.3;


@major-casing-width-z11:          0.3;

@casing-width-z12:                0.1;
@secondary-casing-width-z12:      0.3;
@major-casing-width-z12:          0.5;

@casing-width-z13:                0.5;
@residential-casing-width-z13:    0.5;
@pedestrian-casing-width-z13:     0.2;
@secondary-casing-width-z13:      0.35;
@major-casing-width-z13:          0.5;

@casing-width-z14:                0.55;
@service-casing-width-z14:        0.2;
@secondary-casing-width-z14:      0.35;
@major-casing-width-z14:          0.6;

@casing-width-z15:                0.6;
@secondary-casing-width-z15:      0.7;
@service-casing-width-z15:        0.35;
@major-casing-width-z15:          0.7;

@casing-width-z16:                0.6;
@secondary-casing-width-z16:      0.7;
@minor-service-casing-width-z16:  0.2;
@major-casing-width-z16:          0.7;

@casing-width-z17:                0.8;
@secondary-casing-width-z17:      1;
@major-casing-width-z17:          1;

@casing-width-z18:                0.8;
@secondary-casing-width-z18:      1;
@major-casing-width-z18:          1;

@casing-width-z19:                0.8;
@secondary-casing-width-z19:      1;
@major-casing-width-z19:          1;

@bridge-casing-width-z12:         0.1;
@major-bridge-casing-width-z12:   0.5;
@bridge-casing-width-z13:         0.5;
@major-bridge-casing-width-z13:   0.5;
@bridge-casing-width-z14:         0.5;
@service-bridge-casing-width-z14: 0.15;
@major-bridge-casing-width-z14:   0.6;
@bridge-casing-width-z15:         0.75;
@service-bridge-casing-width-z15: 0.4;
@major-bridge-casing-width-z15:   0.75;
@bridge-casing-width-z16:         0.75;
@minor-service-bridge-casing-width-z16: 0.35;
@major-bridge-casing-width-z16:   0.75;
@bridge-casing-width-z17:         0.8;
@major-bridge-casing-width-z17:   1;
@bridge-casing-width-z18:         0.8;
@major-bridge-casing-width-z18:   1;
@bridge-casing-width-z19:         0.8;
@major-bridge-casing-width-z19:   1;

@paths-background-width:          1;
@paths-bridge-background-width-z18: 2;
@paths-tunnel-background-width-z18: @paths-bridge-background-width-z18;
@paths-bridge-background-width-z19: 3;
@paths-tunnel-background-width-z19: @paths-bridge-background-width-z19;
@paths-bridge-casing-width:       0.5;
@paths-tunnel-casing-width:       1;

@junction-text-color:             husl(12, 100%, 30%);
@halo-color-for-minor-road: white;

@motorway-oneway-arrow-color:     darken(@motorway-casing, 25%);
@trunk-oneway-arrow-color:        darken(@trunk-casing, 25%);
@primary-oneway-arrow-color:      darken(@primary-casing, 15%);
@secondary-oneway-arrow-color:    darken(@secondary-casing, 10%);
@tertiary-oneway-arrow-color:     darken(@tertiary-casing, 30%);
@residential-oneway-arrow-color:  darken(@residential-casing, 40%);
@living-street-oneway-arrow-color: darken(@residential-casing, 30%);
@pedestrian-oneway-arrow-color:   darken(@pedestrian-casing, 40%);
@raceway-oneway-arrow-color:      darken(@raceway-fill, 50%);
@footway-oneway-arrow-color:      darken(@footway-fill, 35%);
@steps-oneway-arrow-color:        darken(@steps-fill, 35%);
@cycleway-oneway-arrow-color:     darken(@cycleway-fill, 25%);
@track-oneway-arrow-color:        darken(@track-fill, 15%);
@bridleway-oneway-arrow-color:    darken(@track-fill, 10%);

@shield-size: 9;
@shield-size-z16: 10;
@shield-size-z18: 11;
@shield-spacing: 760;
@shield-min-distance: 40;
@shield-font: @book-fonts;
@shield-clip: false;

/*
---- stations
*/

@station-color: husl(261, 36%, 55%);
@station-text: darken(saturate(@station-color, 15%), 10%);

/*
---- text
*/

@landcover-font-size: 10;
@landcover-font-size-big: 12;
@landcover-font-size-bigger: 15;
@landcover-wrap-width-size: 25;
@landcover-wrap-width-size-big: 35;
@landcover-wrap-width-size-bigger: 45;
@landcover-face-name: @oblique-fonts;

@standard-wrap-width: 30;
@standard-halo-fill: husla(0, 100%, 100%, 0.6);

@poi-text-color: darken(@poi-color, 15%);

/*
---- water
*/

@water-text: darken(@water-color, 40%);
@water-tunnel-fill: lighten(@water-color, 25%);
@water-bridge-casing: darken(@water-color, 50%);
@glacier: lighten(spin(@water-color, -5), 25%);
@glacier-line: darken(@glacier, 30%);

@breakwater-color: husl(0, 0%, 67%); /* Also for groyne */
@dam: husl(0, 0%, 68%);
@dam-line: husl(0, 0%, 27%);
@weir-line: husl(0, 0%, 67%);
@lock-gate: husl(0, 0%, 67%);
@lock-gate-line: husl(0, 0%, 67%);
