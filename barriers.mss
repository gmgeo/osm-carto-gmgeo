#cliffs {
  [natural = 'cliff'][zoom >= 13] {
    line-pattern-file: url('symbols/cliff.png');
    [zoom >= 15] {
      line-pattern-file: url('symbols/cliff2.png');
    }
  }
  [man_made = 'embankment'][zoom >= 15]::man_made {
    line-pattern-file: url('symbols/embankment.png');
  }
}

#area-barriers {
  [zoom >= 16] {
    opacity: .9;
    line-color: @barrier;
    line-width: 0.4;
    [feature = 'barrier_hedge'],
    [feature = 'barrier_hedge_bank'] {
      polygon-fill: @trees;
    }
  }
}

.barriers {
  [zoom >= 16] {
    line-width: 0.4;
    line-color: @barrier;
  }
  [feature = 'barrier_embankment'][zoom >= 14] {
    line-width: 0.4;
    line-color: @barrier;
  }
  [feature = 'barrier_hedge'][zoom >= 16] {
    opacity: .9;
    line-width: 3;
    line-color: @trees;
  }
  [feature = 'historic_citywalls'],
  [feature = 'barrier_city_wall'] {
    [zoom >= 15] {
      line-color: @barrier;
      line-width: 1.5;
      [zoom >= 17] {
        line-width: 3;
      }
    }
  }
}

#text-line {
  [feature = 'natural_cliff'][zoom >= 15],
  [feature = 'man_made_embankment'][zoom >= 15] {
    text-name: "[name]";
    text-halo-radius: 1;
    text-halo-fill: @standard-halo-fill;
    text-fill: #999;
    text-size: 10;
    text-face-name: @book-fonts;
    text-placement: line;
    text-dy: 8;
    text-vertical-alignment: middle;
    text-spacing: 400;
  }
}
