#buildings {
  [zoom >= 14] {
    [type = 'normal'] {
      polygon-fill: @building-fill-z14;
      polygon-clip: false;
      line-width: @building-line-width-z14;
      line-color: @building-line-z14;
      line-clip: false;
      [building = 'construction'] {
        line-join: round;
        line-dasharray: 1,4;
        polygon-fill: @building-construction-fill-z14;
      }
      [building = 'ruins'] {
        line-dasharray: 6,6;
        polygon-fill: @building-ruins-fill-z14;
      }
      [zoom >= 15] {
        line-color: @building-line-z15;
        polygon-fill: @building-fill;
        [building = 'construction'] {
          polygon-fill: @building-construction-fill;
        }
        [building = 'ruins'] {
          polygon-fill: @building-ruins-fill;
        }
        line-width: @building-line-width;
        line-clip: false;
      }
      [zoom >= 16] {
        line-color: @building-line-z16;
      }
      [zoom >= 17] {
        line-color: @building-line-z17;
      }
    }
    [type = 'major'] {
      polygon-fill: @building-major-fill;
      polygon-clip: false;
      [zoom >= 15] {
        line-width: @building-line-width;
        line-clip: false;
        line-color: @building-major-line-z15;
      }
      [zoom >= 16] {
        line-color: @building-major-line-z16;
      }
      [zoom >= 17] {
        line-color: @building-major-line-z17;
      }
    }
  }
}

#bridge {
  [zoom >= 12] {
    polygon-fill: #B8B8B8;
  }
}
