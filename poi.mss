/* Note that .points is also used in water-features.mss */
.points {
  [feature = 'highway_bus_stop'] {
    [zoom >= 16] {
      marker-file: url('symbols/square.svg');
      marker-fill: @transportation-icon;
      marker-placement: interior;
      marker-width: 6;
      marker-clip: false;
    }
    [zoom >= 17] {
      marker-file: url('symbols/poi/transport/bus-stop-14.svg');
      marker-width: 14;
      marker-fill: @poi-color;
    }
  }

  [feature = 'amenity_bus_station'][zoom >= 16] {
    marker-file: url('symbols/poi/transport/bus-station-18.svg');
    marker-placement: interior;
    marker-clip: false;
    marker-fill: @poi-color;
  }

  [feature = 'tourism_viewpoint'][zoom >= 16] {
    marker-file: url('symbols/poi/tourism/viewpoint-18.svg');
    marker-placement: interior;
    marker-fill: @poi-color;
    marker-clip: false;
  }

  [feature = 'tourism_picnic_site'][zoom >= 16] {
    marker-file: url('symbols/poi/outdoor/table-18.svg');
    marker-fill: @poi-color;
    marker-placement: interior;
    marker-clip: false;
  }

  [feature = 'aeroway_helipad'][zoom >= 16] {
    marker-file: url('symbols/helipad.svg');
    marker-placement: interior;
    marker-clip: false;
    marker-fill: @poi-color;
  }

  [feature = 'natural_spring'][zoom >= 14] {
    marker-file: url('symbols/spring.svg');
    marker-placement: interior;
    marker-clip: false;
  }
}

.amenity-low-priority {
  [railway = 'level_crossing'][zoom >= 14]::railway {
    point-file: url('symbols/level_crossing.svg');
    point-placement: interior;
    [zoom >= 16] {
      point-file: url('symbols/level_crossing2.svg');
    }
  }

  [highway = 'mini_roundabout'][zoom >= 16]::highway {
    marker-file: url('symbols/mini_roundabout.svg');
    marker-placement: interior;
    marker-clip: false;
  }

  [barrier = 'gate']::barrier {
    [zoom >= 17] {
      marker-file: url('symbols/gate.svg');
      marker-placement: interior;
      marker-clip: false;
    }
  }

  [barrier = 'lift_gate'][zoom >= 17]::barrier,
  [barrier = 'swing_gate'][zoom >= 17]::barrier {
    marker-file: url('symbols/liftgate.svg');
    marker-fill: #3f3f3f;
    marker-placement: interior;
    marker-clip: false;
  }

  [barrier = 'bollard'],
  [barrier = 'block'] {
    [zoom >= 17] {
      marker-width: 3;
      marker-line-width: 0;
      marker-fill: #7d7c7c;
      marker-placement: interior;

      [zoom >= 18] {
        marker-width: 4;
      }
    }
  }

  [amenity = 'bench'][zoom >= 19]::amenity {
    marker-file: url('symbols/poi/outdoor/bench-18.svg');
    marker-fill: @poi-color;
    marker-placement: interior;
  }

  [amenity = 'waste_basket'][zoom >= 19]::amenity {
    marker-file: url('symbols/poi/outdoor/waste-basket-14.svg');
    marker-fill: @poi-color;
    marker-placement: interior;
  }

  [leisure = 'picnic_table'][zoom >= 19]::leisure {
    marker-file: url('symbols/poi/outdoor/table-18.svg');
    marker-fill: @poi-color;
    marker-placement: interior;
    marker-clip: false;
  }
}

#poi-poly,
#poi-point {

  // markers with text (shields)

  [feature = 'aeroway_aerodrome'][zoom >= 10][zoom < 17],
  [feature = 'amenity_bicycle_rental'][zoom >= 17],
  [feature = 'amenity_car_rental'][zoom >= 17],
  [feature = 'amenity_fountain'][zoom >= 19],
  [feature = 'amenity_hospital'][zoom >= 15],
  [feature = 'amenity_place_of_worship'][zoom >= 16],
  [feature = 'amenity_shelter'][zoom >= 16],
  [feature = 'amenity_townhall'][zoom >= 16],
  [feature = 'historic_archaeological_site'][zoom >= 16],
  [feature = 'historic_memorial'][zoom >= 17],
  [feature = 'historic_monument'][zoom >= 16],
  [feature = 'man_made_lighthouse'][zoom >= 15],
  [feature = 'man_made_windmill'][zoom >= 16],
  [feature = 'natural_cave_entrance'][zoom >= 15],
  [feature = 'natural_peak'][zoom >= 11],
  [feature = 'natural_saddle'][zoom >= 13],
  [feature = 'natural_volcano'][zoom >= 11],
  [feature = 'tourism_alpine_hut'][zoom >= 13],
  [feature = 'tourism_camp_site'][zoom >= 16],
  [feature = 'tourism_caravan_site'][zoom >= 16],
  [feature = 'tourism_information_office'][zoom >= 17] {
    shield-face-name: @book-fonts;
    shield-fill: @poi-color;
    shield-size: 9;
    shield-text-dx: 9;
    shield-text-dy: 11;
    shield-wrap-width: @standard-wrap-width;
    shield-halo-fill: @standard-halo-fill;
    shield-halo-radius: 1.5;
    shield-placement-type: simple;
    shield-placements: 'S,E,W';
    shield-unlock-image: true;

    [feature = 'aeroway_aerodrome'] {
      shield-file: url('symbols/poi/transport/airport-18.svg');
      [iata != null][zoom >= 10][zoom < 13] {
        shield-name: [iata];
      }
      [zoom >= 12] {
        shield-name: [name];
        [name != null][iata != null] {
          shield-name: [name] + '\n' + [iata];
        }
        [name = null][iata != null] {
          shield-name: [iata];
        }
      }
    }
    [feature = 'amenity_shelter'] {
      shield-file: url('symbols/shelter2.p.16.png');
      shield-name: [name];
    }
    [feature = 'amenity_car_rental'] {
      shield-file: url('symbols/poi/transport/rental-car-18.svg');
      shield-name: [name];
    }
    [feature = 'amenity_fountain'] {
      shield-file: url('symbols/poi/outdoor/fountain-18.svg');
      shield-name: [name];
    }
    [feature = 'amenity_bicycle_rental'] {
      shield-file: url('symbols/poi/transport/rental-bicycle-18.svg');
      shield-name: [name];
    }
    [feature = 'amenity_hospital'] {
      shield-file: url('symbols/poi/health/hospital-18.svg');
      shield-name: [name];
    }
    [feature = 'amenity_townhall'] {
      shield-file: url('symbols/poi/administration/town-hall-18.svg');
      shield-name: [name];
    }
    [feature = 'historic_memorial'] {
      shield-file: url('symbols/poi/tourism/memorial-14.svg');
      shield-name: [name];
    }
    [feature = 'historic_monument'] {
      shield-file: url('symbols/poi/tourism/monument-18.svg');
      shield-name: [name];
    }
    [feature = 'historic_archaeological_site'] {
      shield-file: url('symbols/poi/tourism/archaeological-site-18.svg');
      shield-name: [name];
    }
    [feature = 'man_made_lighthouse'] {
      shield-file: url('symbols/poi/outdoor/lighthouse-18.svg');
      shield-name: [name];
    }
    [feature = 'man_made_windmill'] {
      shield-file: url('symbols/poi/tourism/windmill-18.svg');
      shield-name: [name];
    }
    [feature = 'natural_cave_entrance'] {
      shield-file: url('symbols/poi_cave.p.16.png');
      shield-name: [name];
    }
    [feature = 'tourism_information_office'] {
      shield-file: url('symbols/poi/tourism/information-18.svg');
      shield-name: [name];
    }
    [feature = 'tourism_camp_site'] {
      shield-file: url('symbols/poi/outdoor/camping-18.svg');
      shield-name: [name];
    }
    [feature = 'tourism_caravan_site'] {
      shield-file: url('symbols/poi/outdoor/caravan-18.svg');
      shield-name: [name];
    }
    [feature = 'tourism_alpine_hut'] {
      shield-file: url('symbols/alpinehut.p.16.png');
      [zoom >= 14] {
        shield-name: [name];
        [name != null][elevation != null] {
          shield-name: [name] + '\n' + [elevation];
        }
        [name = null][elevation != null] {
          shield-name: [elevation];
        }
      }
    }
    [feature = 'amenity_place_of_worship'] {
      [zoom >= 17] {
        shield-name: [name];
      }
      shield-file: url('symbols/poi/religious/place-of-worship-18.svg');
      [religion = 'christian'] {
        shield-file: url('symbols/poi/religious/christian-18.svg');
        [denomination = 'jehovahs_witness']{
          shield-file: url('symbols/poi/religious/place-of-worship-18.svg');
        }
      }
      [religion = 'muslim'] {
        shield-file: url('symbols/poi/religious/muslim-18.svg');
      }
      [religion = 'sikh'] {
        shield-file: url('symbols/poi/religious/sikhist-18.svg');
      }
      [religion = 'jewish'] {
        shield-file: url('symbols/poi/religious/jewish-18.svg');
      }
      [religion = 'hindu'] {
        shield-file: url('symbols/poi/religious/hinduist-18.svg');
      }
      [religion = 'buddhist'] {
        shield-file: url('symbols/poi/religious/buddhist-18.svg');
      }
      [religion = 'shinto'] {
        shield-file: url('symbols/poi/religious/shintoist-18.svg');
      }
      [religion = 'taoist'] {
        shield-file: url('symbols/poi/religious/taoist-18.svg');
      }
    }
    [feature = 'natural_peak'],
    [feature = 'natural_saddle'],
    [feature = 'natural_volcano'] {
      shield-min-distance: 50;
      shield-text-dy: 8;

      [feature = 'natural_peak'],
      [feature = 'natural_volcano'] {
        shield-file: url('symbols/poi/nature/peak-14.svg');
      }
      [feature = 'natural_saddle'] {
        shield-file: url('symbols/poi/nature/saddle-14.svg');
      }

      shield-name: [name];
      [name != null][elevation != null][zoom >= 13] {
        shield-name: [name] + '\n' + [elevation];
      }
      [name = null][elevation != null][zoom >= 13] {
        shield-name: [elevation];
      }
    }
  }

  // markers without text

  [feature = 'amenity_drinking_water'][zoom >= 17],
  [feature = 'amenity_hunting_stand'][zoom >= 16],
  [feature = 'amenity_toilets'][zoom >= 17],
  [feature = 'emergency_phone'][zoom >= 17],
  [feature = 'highway_elevator'][zoom >= 18],
  [feature = 'highway_ford'][zoom >= 16],
  [feature = 'man_made_mast_communication'][zoom >= 17],
  [feature = 'man_made_water_tower'][zoom >= 17],
  [feature = 'power_generator_wind'][zoom >= 15],
  [feature = 'tourism_guidepost'][zoom >= 18] {
    marker-fill: @poi-color;
    marker-placement: interior;
    marker-clip: false;

    [feature = 'amenity_drinking_water'] {
      marker-file: url('symbols/poi/outdoor/drinking-water-18.svg');
    }
    [feature = 'amenity_hunting_stand'] {
      marker-file: url('symbols/poi/outdoor/hunting-stand-18.svg');
    }
    [feature = 'amenity_toilets'] {
      marker-file: url('symbols/poi/amenity/toilets-18.svg');
    }
    [feature = 'emergency_phone'] {
      marker-file: url('symbols/poi/emergency/emergency-phone-18.svg');
    }
    [feature = 'highway_elevator'] {
      marker-file: url('symbols/poi/transport/elevator-18.svg');
    }
    [feature = 'highway_ford'] {
      marker-file: url('symbols/poi/transport/ford-18.svg');
    }
    [feature = 'man_made_mast_communication'] {
      marker-file: url('symbols/poi/outdoor/mast-communications-18.svg');
    }
    [feature = 'man_made_water_tower'] {
      marker-file: url('symbols/poi/outdoor/water-tower-18.svg');
    }
    [feature = 'power_generator_wind'] {
      marker-file: url('symbols/poi/energy/power-wind-18.svg');
    }
    [feature = 'tourism_guidepost'] {
      marker-file: url('symbols/poi/outdoor/guidepost-18.svg');
    }
  }
}

#text-poly,
#text-point {
  [feature = 'tourism_picnic_site'][zoom >= 17],
  [feature = 'tourism_viewpoint'][zoom >= 16],
  [feature = 'highway_bus_stop'][zoom >= 17],
  [feature = 'amenity_bus_station'][zoom >= 17] {
    text-name: "[name]";
    text-size: 9;
    text-fill: @poi-text-color;
    text-face-name: @book-fonts;
    text-halo-radius: 1;
    text-halo-fill: @standard-halo-fill;
    text-wrap-width: @standard-wrap-width;
    text-placement: interior;

    [feature = 'tourism_picnic_site'] {
      text-dy: 10;
    }
    [feature = 'tourism_viewpoint'],
    [feature = 'amenity_bus_station'] {
      text-dy: 11;
    }
    [feature = 'highway_bus_stop'] {
      text-dy: 9;
    }
  }

  [feature = 'aeroway_helipad'][zoom >= 16] {
    text-name: "[name]";
    text-size: 8;
    text-fill: @poi-color;
    text-dy: -10;
    text-face-name: @bold-fonts;
    text-halo-radius: 1;
    text-halo-fill: @standard-halo-fill;
    text-placement: interior;
    text-wrap-width: @standard-wrap-width;
  }

  [feature = 'natural_tree'][zoom >= 17] {
    text-name: "[name]";
    text-size: 9;
    text-fill: green;
    text-dy: 7;
    [zoom >= 18] { text-dy: 8; }
    [zoom >= 19] { text-dy: 11; }
    [zoom >= 20] { text-dy: 18; }
    text-face-name: @book-fonts;
    text-halo-radius: 1;
    text-halo-fill: @standard-halo-fill;
    text-placement: interior;
    text-wrap-width: @standard-wrap-width;
  }
}
