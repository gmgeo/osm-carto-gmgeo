/* For performance reasons, the admin border layers are split into three groups
for low, middle and high zoom levels.
For each zoomlevel, all borders come from a single attachment, to handle
overlapping borders correctly.
*/

#admin-low-zoom[zoom >= 4][zoom < 11],
#admin-mid-zoom[zoom >= 11] {
  [admin_level = '2'] {
    [zoom >= 4] {
      background/line-join: bevel;
      background/line-color: white;
      background/line-width: 1.2;
      line-join: bevel;
      line-color: @admin-boundaries;
      line-width: 1.2;
    }
    [zoom >= 5] {
      background/line-width: 1.6;
      line-width: 1.6;
    }
    [zoom >= 6] {
      background/line-width: 1.8;
      line-width: 1.8;
    }
    [zoom >= 7] {
      background/line-width: 2;
      line-width: 2;
    }
    [zoom >= 10] {
      background/line-width: 6;
      line-width: 6;
    }
  }
  [admin_level = '3'] {
    [zoom >= 4] {
      background/line-join: bevel;
      background/line-color: white;
      background/line-width: 0.6;
      line-join: bevel;
      line-color: @admin-boundaries;
      line-width: 0.6;
    }
    [zoom >= 7] {
      background/line-width: 1.2;
      line-width: 1.2;
    }
    [zoom >= 10] {
      background/line-width: 4;
      line-width: 4;
      line-dasharray: 4,2;
      line-clip: false;
    }
  }
  [admin_level = '4'] {
    [zoom >= 4] {
      background/line-join: bevel;
      background/line-color: white;
      background/line-width: 0.4;
      line-join: bevel;
      line-color: @admin-boundaries;
      line-width: 0.4;
      line-dasharray: 4,3;
      line-clip: false;
    }
    [zoom >= 5] {
      background/line-width: 0.6;
      line-width: 0.6;
    }
    [zoom >= 6] {
      background/line-width: 0.8;
      line-width: 0.8;
    }
    [zoom >= 7] {
      background/line-width: 1;
      line-width: 1;
    }
    [zoom >= 9] {
      background/line-width: 1.8;
      line-width: 1.8;
    }
    [zoom >= 10] {
      background/line-width: 2.5;
      line-width: 2.5;
    }
    [zoom >= 11] {
      background/line-width: 3;
      line-width: 3;
    }
  }
  /*
  The following code prevents admin boundaries from being rendered on top of
  each other. Comp-op works on the entire attachment, not on the individual
  border. Therefore, this code generates an attachment containing a set of
  @admin-boundaries/white dashed lines (of which only the top one is visible),
  and with `comp-op: darken` the white part is ignored, while the
  @admin-boundaries colored part is rendered (as long as the background is not
  darker than @admin-boundaries).
  The SQL has `ORDER BY admin_level`, so the boundary with the lowest
  admin_level is rendered on top, and therefore the only visible boundary.
  */
  opacity: 0.4;
  comp-op: darken;
}

#admin-mid-zoom[zoom >= 11] {
  [admin_level = '5'][zoom >= 11] {
    background/line-join: bevel;
    background/line-color: white;
    background/line-width: 2;
    line-join: bevel;
    line-color: @admin-boundaries;
    line-width: 2;
    line-dasharray: 5,2;
    line-clip: false;
  }
  [admin_level = '6'][zoom >= 11] {
    background/line-join: bevel;
    background/line-color: white;
    background/line-width: 2;
    line-join: bevel;
    line-color: @admin-boundaries;
    line-width: 2;
    line-dasharray: 5,2;
    line-clip: false;
  }
  opacity: 0.5;
  comp-op: darken;
}

#admin-text[zoom >= 16] {
  text-name: "[name]";
  text-face-name: @book-fonts;
  text-fill: @admin-boundaries;
  text-halo-radius: 1.0;
  text-halo-fill: @standard-halo-fill;
  text-placement: line;
  text-clip: true;
  text-vertical-alignment: middle;
  text-dy: -10;
}

#nature-reserve-text[zoom >= 13][way_pixels > 192000] {
  text-name: "[name]";
  text-face-name: @book-fonts;
  text-fill: @national-park-boundaries-text;
  text-halo-radius: 1.0;
  text-halo-fill: @standard-halo-fill;
  text-placement: line;
  text-clip: true;
  text-vertical-alignment: middle;
  text-size: 10;
  text-dy: -11;

  [zoom >= 16] {
    text-size: 11;
  }
  [zoom >= 18] {
    text-size: 12;
  }

  [zoom >= 14] {
    text-dy: -15;
  }
  [zoom >= 16] {
    text-dy: -22;
  }
  [zoom >= 18] {
    text-dy: -26;
  }
}

#nature-reserve-boundaries {
  [way_pixels > 100][zoom >= 7] {
    [zoom < 10] {
      ::fill {
        opacity: 0.05;
        polygon-fill: @national-park-boundaries;
      }
    }
    a/line-width: 1;
    a/line-offset: -0.5;
    a/line-color: @national-park-boundaries;
    a/line-opacity: 0.15;
    a/line-join: round;
    a/line-cap: round;
    b/line-width: 2;
    b/line-offset: -1;
    b/line-color: @national-park-boundaries;
    b/line-opacity: 0.15;
    b/line-join: round;
    b/line-cap: round;
    [zoom >= 10] {
      a/line-width: 1.5;
      a/line-offset: -0.75;
      b/line-width: 3;
      b/line-offset: -1.5;
    }
    [zoom >= 12] {
      a/line-width: 2;
      a/line-offset: -1;
      b/line-width: 4;
      b/line-offset: -2;
    }
    [zoom >= 14] {
      b/line-width: 6;
      b/line-offset: -3;
    }
    [zoom >= 16] {
      a/line-width: 3;
      a/line-offset: -2.5;
      b/line-width: 8;
      b/line-offset: -6;
    }
    [zoom >= 18] {
      a/line-width: 3.5;
      a/line-offset: -4;
      b/line-width: 10;
      b/line-offset: -8;
    }
  }
}
