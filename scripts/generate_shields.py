#!/usr/bin/env python

# generate highway shields

from __future__ import print_function
import copy, lxml.etree, math, os, colorsys, re

def main():

  namespace = 'http://www.w3.org/2000/svg'
  svgns = '{' + namespace + '}'
  svgnsmap = {None: namespace}

  config = {}
  config['base'] = {}

  config['base']['rounded_corners'] = 2
  config['base']['font_height'] = 9.1
  config['base']['font_width'] = 5.9
  config['base']['padding_x'] = 4
  config['base']['padding_y'] = 2
  config['base']['fill'] = '#ddd'
  config['base']['stroke_width'] = 1
  config['base']['stroke_fill'] = '#000'

  config['global'] = {}

  config['global']['types'] = ['motorway', 'trunk', 'primary', 'secondary', 'tertiary']
  config['global']['max_width'] = 11
  config['global']['max_height'] = 4
  config['global']['output_dir'] = '../symbols/shields/' # specified relative to the script location

  config['global']['additional_sizes'] = ['base', 'z16', 'z18']

  # specific values overwrite config['base'] ones
  config['motorway'] = {}
  config['trunk'] = {}
  config['primary'] = {}
  config['secondary'] = {}
  config['tertiary'] = {}


  config['motorway']['fill'] = 'husl(359, 57%, 85%)'
  config['motorway']['stroke_fill'] = 'husl(359, 42%, 60%)'
  config['trunk']['fill'] = 'husl(31, 62%, 88%)'
  config['trunk']['stroke_fill'] = 'husl(31, 47%, 65%)'
  config['primary']['fill'] = 'husl(60, 55%, 93%)'
  config['primary']['stroke_fill'] = 'husl(60, 30%, 63%)'
  config['secondary']['fill'] = 'husl(86, 21%, 97%)'
  config['secondary']['stroke_fill'] = 'husl(86, 14%, 68%)'
  config['tertiary']['fill'] = 'husl(0, 0%, 96%)'
  config['tertiary']['stroke_fill'] = 'husl(0, 0%, 70%)'

  # changes for different size versions
  config['z16'] = {}
  config['z18'] = {}

  config['z16']['font_width'] = 6.5
  config['z16']['font_height'] = 10.1
  config['z18']['font_width'] = 7.2
  config['z18']['font_height'] = 11.1

  if not os.path.exists(os.path.dirname(config['global']['output_dir'])):
    os.makedirs(os.path.dirname(config['global']['output_dir']))

  for height in range(1, config['global']['max_height'] + 1):
    for width in range(1, config['global']['max_width'] + 1):
      for shield_type in config['global']['types']:

        # merge base config and specific styles
        vars = copy.deepcopy(config['base'])
        if shield_type in config:
          for option in config[shield_type]:
            vars[option] = config[shield_type][option]

        for shield_size in config['global']['additional_sizes']:

          if shield_size != 'base':
            if shield_size in config:
              for option in config[shield_size]:
                vars[option] = config[shield_size][option]

          shield_width = 2 * vars['padding_x'] + math.ceil(vars['font_width'] * width)
          shield_height = 2 * vars['padding_y'] + math.ceil(vars['font_height'] * height)

          svg = lxml.etree.Element('svg', nsmap=svgnsmap)
          svg.set('width', '100%')
          svg.set('height', '100%')
          svg.set('viewBox', '0 0 ' + str(shield_width  + vars['stroke_width']) + ' ' + str(shield_height + vars['stroke_width']))

          if vars['stroke_width'] > 0:
            offset_x = vars['stroke_width'] / 2.0
            offset_y = vars['stroke_width'] / 2.0
          else:
            offset_x = 0
            offset_y = 0

          shield = lxml.etree.Element(svgns + 'rect')
          shield.set('x', str(offset_x))
          shield.set('y', str(offset_y))
          shield.set('width', str(shield_width))
          shield.set('height', str(shield_height))
          if vars['rounded_corners'] > 0:
            shield.set('rx', str(vars['rounded_corners']))
            shield.set('ry', str(vars['rounded_corners']))
          shield.set('id', 'shield')

          stroke = ''
          if vars['stroke_width'] > 0:
            stroke = 'stroke:' + parseColor(vars['stroke_fill']) + ';stroke-width:' + str(vars['stroke_width']) + ';'

          shield.set('style', 'fill:' + parseColor(vars['fill']) + ';' + stroke)

          svg.append(shield)

          filename = shield_type + '_' + str(width) + 'x' + str(height)
          if shield_size != 'base':
            filename = filename + '_' + shield_size

          filename = filename + '.svg'

          # save file
          try:
            shieldfile = open(os.path.join(os.path.dirname(__file__), config['global']['output_dir'] + filename), 'w')
            shieldfile.write(lxml.etree.tostring(svg, encoding='utf-8', xml_declaration=True, pretty_print=True))
            shieldfile.close()
          except IOError:
            print('Could not save file ' + filename + '.')
            continue
  return

# parse rgb, hex, hsl and husl (perceptual) color definitions, return hex color or None on error
# husl needs husl library, load on demand
def parseColor(color):
    parsed_color = None
    if color.startswith('#'):
        if re.match('^#[0-9a-f]{6}$', color) != None:
            parsed_color = color
    elif color.startswith('rgb'):
        rgb_match = re.search('^rgb\(([0-9]{1,3}),\s*([0-9]{1,3}),\s*([0-9]{1,3})\)$', color)
        if rgb_match is not None:
            try:
                r = min(int(rgb_match.group(1)), 255)
                g = min(int(rgb_match.group(2)), 255)
                b = min(int(rgb_match.group(3)), 255)
                parsed_color = '#'+("%x" % r)+("%x" % g)+("%x" % b)
            except ValueError:
                pass
    elif color.startswith('hsl'):
        hsl_match = re.search('^hsl\(([0-9]{1,3}(\.[0-9]*)?),\s*([0-9]{1,3}(\.[0-9]*)?)\%,\s*([0-9]{1,3}(\.[0-9]*)?)\%\)$', color)
        if hsl_match is not None:
            try:
                h = min(float(hsl_match.group(1)) / 365, 1)
                s = min(float(hsl_match.group(3)) / 100, 1)
                l = min(float(hsl_match.group(5)) / 100, 1)
                (r, g, b) = colorsys.hls_to_rgb(h, l, s)
                parsed_color = '#'+("%x" % (r * 255))+("%x" % (g * 255))+("%x" % (b * 255))
            except ValueError:
                pass
    elif color.startswith('husl'):
        husl_match = re.search('^husl\(([0-9]{1,3}(\.[0-9]*)?),\s*([0-9]{1,3}(\.[0-9]*)?)\%,\s*([0-9]{1,3}(\.[0-9]*)?)\%\)$', color)
        if husl_match is not None:
            try:
                import husl
                h = min(float(husl_match.group(1)), 360)
                s = min(float(husl_match.group(3)), 100)
                l = min(float(husl_match.group(5)), 100)
                rgb = husl.husl_to_rgb(h, s, l)
                parsed_color = husl.rgb_to_hex(rgb)
            except ValueError:
                pass
            except ImportError:
                print('HUSL colour definitions need the husl library. Please install it.')
                pass

    return parsed_color

if __name__ == "__main__": main()
