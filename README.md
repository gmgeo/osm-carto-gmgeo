# OSM Carto GMGeo

This style has been forked from [OpenStreetMap Carto](https://github.com/gravitystorm/openstreetmap-carto) and is based on many hours of hard work from a lot of people. From Nov 2015 on it was modified by me and is used mainly as my
playground for trying out ideas that should nevertheless lead to a good looking style.

I'll try to keep track of new developments within openstreetmap-carto and incorporate them here as I see fit. But essentially this is a new style.

While you are free to report (and fix!) bugs there is no guarantee that I will implement any particular feature you might request.
However, feedback is welcome.

It comes with no warranty or guarantees whatsoever and is licensed in the same license as the original style it has been forked from, which is CC0 (Public Domain).

## Dependencies

* [omniscale/magnacarto](https://github.com/omniscale/magnacarto) HEAD or [mapbox/carto](https://github.com/mapbox/carto) >= 0.16.0
